---
layout: page
title: Resources
permalink: /resources/
---
Resources for Learning Software Skills

The following is a set of resources for people looking to learn some coding skills.

### Command Line Tools
One good place to get started is an introduction to the command line. While not 100% required to learn to code, all of the best software engineers I know are highly proficient in it. [I'd this tutorial](https://www.learnenough.com/command-line-tutorial)

### General Programming
If you just want to dive in and learn some coding. [Code Combat](https://codecombat.com/) is an interesting way to start. [My wife](http://courtney.ives.mn) got her start there and she enjoyed it. If you want to try and study it, I'd take a look at [Treehouse](https://teamtreehouse.com/). They've got a bunch of diiferent tracks you can follow to learn different skills. In general there are some good [tutorials and courses here.](https://www.learnenough.com/courses)

### Git
There are a couple of good git tutorials out there sort of along the same lines. [Try-git from github](https://try.github.io/levels/1/challenges/1) is pretty good, but sort of limited/shallow in scope. If you're just getting started it's a fast way to learn some commands. [Learn git branching](http://learngitbranching.js.org/) is a bit more in depth and talks about the git branching model. That's important if you work on teams. I'd also take a gander (and then bookmark) [Oh Shit Git!](http://ohshitgit.com). It's got a bunch of answers to problems you might encounter.

### Ruby/Rails
If you're looking for something a little bit more practical, the [rails bridge curriculum](http://curriculum.railsbridge.org/docs/) is a very good direction to go. After the "Installfest" section, each of the sections under rails has a small project that could be finished in a day. Their site is full of really uesful information. Also, there are RailsBridge workshops that happen all over the country. I'd also suggest the [Michael Hartl rails tutorial](https://www.railstutorial.org/book)

### SQL/Postgres
If you have wants/needs to learn sql, this [site seems to have some interesting exercises](https://pgexercises.com/gettingstarted.html).

### Regular Expressions
If you want to learn about regular expressions, doing [regex crosswords](https://regexcrossword.com/) are kind of fun and will really hone your skills.
