# Pre-install most gems to make tests run faster
FROM ruby:2.6-alpine

COPY Gemfile* /app/

WORKDIR /app

RUN apk add --no-cache \
      make \
      gcc \
      linux-headers \
      curl \
      g++ && \
    bundle install --system
