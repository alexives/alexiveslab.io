---
layout: project
title:  Building a Counterflow Wort Chiller
subtitle: It's pretty easy actually
date:   2018-12-07 16:00
categories: DIY homebrewing
author: Alex Ives
---

I started homebrewing about 8 years ago, and since then I've steadily upgraded my brewing setup, recently I finally got around to adding a ball valve to my kettle and went to brew a batch only to realize that my wort chiller had super oxidized and I didn't want to deal with it!

So I decided he, what a good chance to build a new one! It turned out to be pretty inexpensive to build one.

### Parts List ~$40:

- 2x 1/2" ID Barb x 1/2" MIP Brass Adapter
- 2x 1/2" FIP Brass Tee
- 1x 3/4" Female Garden Hose x 1/2" MIP Brass Adapter
- 1x 3/4" Male Garden Hose x 1/2" MIP Brass Adapter
- 1x 20' x 3/8" copper tube
- 1x 25' Garden Hose
- 1x Package of JB Weld

### Tools:

- Drill with 3/8" bit
- Copper Pipe Cutter
- Corny Keg

### Steps:

1. Trim the hose to about 19'.
2. Straighten the copper tube.
3. Feed the copper through hose.
4. Drill out the inside of the barbed adapter with a 3/8" drill bit.
5. Coil the hose around a corny keg.
6. Make sure there are at least 6 inches of brass pipe at each end of the hose.
7. Put a 1/4" bead of JB Weld at each end of the hose and fit a brass Tee at each end over the hose with the JB Weld. (Don't add the barbed fitting yet, I did stuff in a bad order.)
![Barb and Tee Fitted](/images/chiller/barbAndTeeFitted.jpg)
8. Put a bead of JB Weld on the inside of the Brass Barb and thread tape on the threads, then fit the barb over the copper pipe and screw it in to the Tee.
![JB Weld on Barb](/images/chiller/jbWeldBarb.jpg)
![Thread Tape on Barb](/images/chiller/pipeTapeBarb.jpg)
9. Let the JB Weld Cure over night.
![Cure Overnight](/images/chiller/cureOvernight.jpg)
10. Put thread tape on the threads of the hose adapters and add them to the Tees at the end of the hose.
11. Wait at least 15 Hours after applying the JB Weld before using it the first time so the JB Weld has time to cure entirely.
