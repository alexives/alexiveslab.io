---
layout: project
title:  Easy Switch Tester/Fidgets
date:   2017-11-12 18:16
categories: keyboards gifts
author: Alex Ives
---

As the holidays roll around every year I always try to think of was that I can give people cool gifts that really come from me and somehow are still cool. I also like making stuff. This has inevitably ended in several duds. This year however, I saw something on Massdrop and thought, hey, I bet I could make something like that and it wouldn't cost me $20. (TBH nor do mine look as nice.) The [ClickeyBits fidgets](https://www.massdrop.com/buy/clickeybits-fidgety-switch-tester?referer=X3AMM6&mode=guest_open) are kind of a neat idea!

I also happened to be working on the [MittensKB Rev 2 case](/keyboards/projects/2017/12/22/I-built-a-keyboard/) at the same time, so I figured I could cut out some boxes inside of some of the bigger holes in the case. It's a pretty simple design:
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0" y="0" width="263.016" height="187.344" viewBox="0, 0, 263.016, 187.344" alt="design for cutting keyswitch fidgets">
  <g id="Layer_1">
    <path d="M33.134,18 L33.134,26.496 L48.269,26.496 L48.269,18 L63.403,18 L63.403,26.496 L78.537,26.496 L78.537,18 L93.672,18 L93.672,33.134 L85.176,33.134 L85.176,48.269 L93.672,48.269 L93.672,63.403 L85.176,63.403 L85.176,78.537 L93.672,78.537 L93.672,93.672 L78.537,93.672 L78.537,85.176 L63.403,85.176 L63.403,93.672 L48.269,93.672 L48.269,85.176 L33.134,85.176 L33.134,93.672 L18,93.672 L18,78.537 L26.496,78.537 L26.496,63.403 L18,63.403 L18,48.269 L26.496,48.269 L26.496,33.134 L18,33.134 L18,18 L33.134,18 z M75.678,35.993 L35.993,35.993 L35.993,75.678 L75.678,75.678 L75.678,35.993 z" fill-opacity="0" stroke="#000000" stroke-width="0.72"/>
    <path d="M108.806,18 L108.806,26.496 L123.941,26.496 L123.941,18 L139.075,18 L139.075,26.496 L154.21,26.496 L154.21,18 L169.344,18 L169.344,33.134 L160.848,33.134 L160.848,48.269 L169.344,48.269 L169.344,63.403 L160.848,63.403 L160.848,78.537 L169.344,78.537 L169.344,93.672 L154.21,93.672 L154.21,85.176 L139.075,85.176 L139.075,93.672 L123.941,93.672 L123.941,85.176 L108.806,85.176 L108.806,93.672 L93.672,93.672 L93.672,78.537 L102.168,78.537 L102.168,63.403 L93.672,63.403 L93.672,48.269 L102.168,48.269 L102.168,33.134 L93.672,33.134 L93.672,18 L108.806,18 z M151.35,35.993 L111.665,35.993 L111.665,75.678 L151.35,75.678 L151.35,35.993 z" fill-opacity="0" stroke="#000000" stroke-width="0.72"/>
    <path d="M184.478,18 L184.478,26.496 L199.613,26.496 L199.613,18 L214.747,18 L214.747,26.496 L229.882,26.496 L229.882,18 L236.52,18 L236.52,33.134 L245.016,33.134 L245.016,48.269 L236.52,48.269 L236.52,63.403 L245.016,63.403 L245.016,78.537 L236.52,78.537 L236.52,93.672 L229.882,93.672 L229.882,85.176 L214.747,85.176 L214.747,93.672 L199.613,93.672 L199.613,85.176 L184.478,85.176 L184.478,93.672 L177.84,93.672 L177.84,78.537 L169.344,78.537 L169.344,63.403 L177.84,63.403 L177.84,48.269 L169.344,48.269 L169.344,33.134 L177.84,33.134 L177.84,18 L184.478,18 z M227.022,35.993 L187.337,35.993 L187.337,75.678 L227.022,75.678 L227.022,35.993 z" fill-opacity="0" stroke="#000000" stroke-width="0.72"/>
    <path d="M184.478,93.672 L184.478,102.168 L199.613,102.168 L199.613,93.672 L214.747,93.672 L214.747,102.168 L229.882,102.168 L229.882,93.672 L236.52,93.672 L236.52,108.806 L245.016,108.806 L245.016,123.941 L236.52,123.941 L236.52,139.075 L245.016,139.075 L245.016,154.21 L236.52,154.21 L236.52,169.344 L229.882,169.344 L229.882,160.848 L214.747,160.848 L214.747,169.344 L199.613,169.344 L199.613,160.848 L184.478,160.848 L184.478,169.344 L177.84,169.344 L177.84,154.21 L169.344,154.21 L169.344,139.075 L177.84,139.075 L177.84,123.941 L169.344,123.941 L169.344,108.806 L177.84,108.806 L177.84,93.672 L184.478,93.672 z M227.022,111.665 L187.337,111.665 L187.337,151.35 L227.022,151.35 L227.022,111.665 z" fill-opacity="0" stroke="#000000" stroke-width="0.72"/>
    <path d="M48.269,93.672 L48.269,102.168 L63.403,102.168 L63.403,93.672 L78.537,93.672 L78.537,102.168 L85.176,102.168 L85.176,108.806 L93.672,108.806 L93.672,123.941 L85.176,123.941 L85.176,139.075 L93.672,139.075 L93.672,154.21 L85.176,154.21 L85.176,160.848 L78.537,160.848 L78.537,169.344 L63.403,169.344 L63.403,160.848 L48.269,160.848 L48.269,169.344 L33.134,169.344 L33.134,160.848 L26.496,160.848 L26.496,154.21 L18,154.21 L18,139.075 L26.496,139.075 L26.496,123.941 L18,123.941 L18,108.806 L26.496,108.806 L26.496,102.168 L33.134,102.168 L33.134,93.672 L48.269,93.672 z M75.678,111.665 L35.993,111.665 L35.993,151.35 L75.678,151.35 L75.678,111.665 z" fill-opacity="0" stroke="#000000" stroke-width="0.72"/>
    <path d="M123.941,93.672 L123.941,102.168 L139.075,102.168 L139.075,93.672 L154.21,93.672 L154.21,102.168 L160.848,102.168 L160.848,108.806 L169.344,108.806 L169.344,123.941 L160.848,123.941 L160.848,139.075 L169.344,139.075 L169.344,154.21 L160.848,154.21 L160.848,160.848 L154.21,160.848 L154.21,169.344 L139.075,169.344 L139.075,160.848 L123.941,160.848 L123.941,169.344 L108.806,169.344 L108.806,160.848 L102.168,160.848 L102.168,154.21 L93.672,154.21 L93.672,139.075 L102.168,139.075 L102.168,123.941 L93.672,123.941 L93.672,108.806 L102.168,108.806 L102.168,102.168 L108.806,102.168 L108.806,93.672 L123.941,93.672 z M151.35,111.665 L111.665,111.665 L111.665,151.35 L151.35,151.35 L151.35,111.665 z" fill-opacity="0" stroke="#000000" stroke-width="0.72"/>
  </g>
</svg>

From there it was just a matter of cutting them out on the laser and then gluing together the edges like so:

![Partially assembled fidgets with a couple of switches in some of them](/images/keyboards/fidgets/partial.jpeg)

Then you just fill it in with more switches. While they did stay in OK without it, I did glue mine in because I didn't want people to get them and have them just fall apart all the time.

![Fidget cubes with all keyswitch holes filled](/images/keyboards/fidgets/filled.jpeg)

Then I used some of my spare keycaps from the bag of bulk random keycaps I was a sucker enough to buy from PMK last year. Glad that I have them now of course since it's actually kind of nice to be able to just fill these in.

![Fidget cubes fully assembled with keycaps](/images/keyboards/fidgets/complete.jpeg)

At any rate, if you want to build your own, I'd suggest using the template I made here. I used the [MakerCase box generator](http://www.makercase.com/), and then the switch holes from the mittens design that came from the [SwillKB builder](http://builder.swillkb.com/). Your best bet is to download the PDF since PDF has a concept of real world size built into it.

### [Download the plans Here](/files/keyboard/fidget.pdf)