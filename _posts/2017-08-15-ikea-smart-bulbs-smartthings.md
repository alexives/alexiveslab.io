---
layout: post
title:  Using IKEA TRÅDFRI bulbs with SmartThings
date:   2017-08-15 21:16
categories: smart home lights
author: Alex Ives
---
Like most other techies, I think smart home is pretty neat. I've got a couple of echo dots and a first gen smartthings hub. Honestly, didn't ever get much more than that because it's always seemed hard to justify putting stuff like lightswitches into a rental unit, and the cost of hue bulbs was so high that I wasn't willing to invest in all of the stuff I'd need just to light a couple of rooms.

That's changed with the introduction of the new Ikea bulbs. At $12/each for the low end bulbs these things are relatively inexpensie, at least compared to hue bulbs, which while they start at about $25 for two on amazon, you also need an extra device in order to get them working with either an alexa device or a smartthings hub. That sets you back another $60 just to try it out.

I ended up getting 3 of the [cheapest TRÅDFRI bulbs](http://www.ikea.com/us/en/catalog/products/80339436/) and decided to put them in the bedroom. We don't have a dimmer switch in there and it's always annoying to have to get out of bed to turn the overhead lights off.

Set up for those bulbs wasn't too bad but it did take a little doing. You may also want to invest in a simple corded light bulb socket if you don't have one around, the bulbs have to be pretty much touching the hub when you're pairing them and I ended up tipping over a lamp and lifting up the hub in order to get them close enough. [Ikea has one for $6](http://www.ikea.com/us/en/catalog/products/10175810/) that should work pretty well.

### Setting it up in SmartThings
1. Plug in your bulb in the corded socket and turn it on, open the smart things app.
2. In the app, add a new device and just hold the lightbulb right on top of the hub until it shows up. I would suggest adding one at a time to avoid confusion of which one is which.
3. Now, on a computer, go to [your smartthings device list](https://graph.api.smartthings.com/device/list) and select the bulb you just added.
4. Select `Edit` and then under `Type` select `ZLL Dimmer Bulb` and hit `Update`

That's it! It should be all set up and ready to go. This is sort of a pain, but hopefully it'll get added to smartthings officially and you won't have to tell it what kind of thing it is in the future.