---
layout: software
title:  "Timezone workflow for Alfred 4"
head_title: "Timezone workflow for Alfred 4"
date:   2019-12-04 14:16:42
categories: alfred code
icon: images/alfred-timezones/icon.png
author: Alex Ives
repo: https://gitlab.com/alexives/timezone-helper-alfred-workflow/
latest_release: https://gitlab.com/alexives/timezone-helper-alfred-workflow/-/jobs/artifacts/master/raw/Timezone-Helper.alfredworkflow?job=build
---

I guess I'm on an alfred workflow kick right now.

Since I started at GitLab, I've had trouble with timezones. I've got a world clock that gives me all of my teammates timezones in a dropdown, but if I'm trying to schedule time with someone, I have to go do math to figure out if the time I'm scheduling for is a time that they'll reasonably be working.

To fix this, I've written a Timezone Helper workflow. It's easy, if you type `timez` into alfred it gives you a list of configured timezones. If you give it a time afterwards, like `timez 10:00` it gives you the same list, but the times the equivalent to the one you entered. That way you won't accidentally schedule a meeting for someone in the middle of the night!

![Timezone list with timezones for each specified zone](/images/alfred-timezones/list-view.png)

If you want to add a new zone, you can use `timez update <name>, <offset>` and it'll add that name to the list. If you give it an existing name, it'll update it. (This is useful at DST change time).

![Timezone update](/images/alfred-timezones/add-individual.png)

If you want to remove one, you can select it from the list then press ⌘ and enter to remove it.

![update list view](/images/alfred-timezones/update-list.png)

Anyway, if you work on a distrubted team, maybe this'll be useful for you!
