---
layout: post
title:  Selecting a Keyboard Layout
subtitle: Building a keyboard - Part 1
date:   2018-04-14 17:00
categories: keyboards DIY
author: Alex Ives
seqnext: /keyboards/diy/2018/04/14/building-your-own-keyboard-pt-2/
---

The first step in building a keyboard can in some ways be a daunting one. Selecting a layout can be really stressful if you aren't sure what you want. There are a lot of options out there for both DIY and non-DIY options, and most of you will probably only build one or two, so getting a layout you really like is an important step.

The first keyboard I ever built was an ErgoDox, and when I finally got it working I complained about having to reach some of the keys and one of my coworkers said "Why didn't you just outline your hand on a piece of paper and make your keyboard around that?" At the time, I didn't really think that was an option, after all, what did I know about building keyboards? My advice to someone just starting out and reading this is not to worry about it. If your heart is set on a particular layout, just go for it.

Also `<plug>` I'd suggest checking out [Keyboards that click](http://keyboardsthat.click), which has a compendium of a bunch of different layouts and filters so you can sort through them. `</plug>`

There are a variety of considerations, there are basically 4 factors I'd suggest considering when thinking about keyboards. Key stagger, split style, height, and width.


### Height

To start, all of the dimensions for keyboards are measured in a unit called `u`. `1u` = one normal key width. So your standard letters and numbers would be 1u, where as tab is often 1.5u, and backspace is often 2u.

In general, keyboards fall into one of three categories of heights (though there are exceptions) with a minimum height of 4u and a max of 6u. In general, the fewer rows you have, the more you have to rely on shifted layers to get all of the keys you need.

When considering how many rows you want I'd ask yourself the following:

1. Do you feel like you have to really reach for keys and they're far away?
2. Are you comfortable changing your typing habits for a keyboard that makes you feel better?

If you answered yes to both questions I'd say going to a 4u height keyboard might be a good choice for you, using chording (aka layers) means less reaching, but it will impact your typing habits and it takes a little bit of getting used to. Probably a few days to get back up to your typing speed.

If you answered yes to the first question and maybe to the second, I'd suggest looking at a 60% keyboard or something with a 5u height. That moves the f keys to a layer, but for the most part leaves the experience the same as using a normal keyboard.

If you anwered no to both, I'd suggest sticking with a 6u height keyboard, that pretty much locks you into either the full-sized or tenkeyless realms, but there are a few options in that height range.

### Width

Like height, all of the dimensions are measured in `u`.

Keyboard width is one of those things that's pretty debateable. In terms of ergonomics, the general consensus is that moving your hand further to the mouse is less good, so going for a narower keyboard might be good, but to me, it's always seemed like a diminishing return. In terms of desk space, the savings is obvious.

Consider the following:

1. Is your desk small?
2. Do you feel like your mouse is really far away?
3. Do you use the arrow keys a lot?
4. Do you use the numpad a lot?

If you answered Yes to 1 or 2, and no to 4, you can drop right away down to `18u`. If you answered yes to 1, 2, and 4, you may want to consider getting an external number pad, they make some pretty good ones and then you can put your number pad in an ergonomically relevant place.

If you answered yes to 1 or 2 and 3, there are other options if you're using a keyboard with layers, and other location options, especially in the 65% keyboard area (16u x 5u). Another option if you're looking even lower is to take advantage of layering. I actually keep arrow keys on h,j,k,l right on my home row with a modifier, no more traveling down to the arrow cluster (which I got rid of)

### Key Stagger

Traditionally, keyboards have had keys that are staggered horizontally. This tradition dates back to typewriters in order for the key stems not to get in the way of one another, and has no bearing at all on usability. To that end, a lot of ergonomic options these days are pushing either no stagger at all (a grid) or a vertical stagger that lets your fingers move in a straight line rather than all over the place.

Consider the following:

1. Do you feel like your fingers are reaching in strange directions?
2. Are you worried about carpel tunnel or other ergonomic issues?
3. Are you comfortable changing your typing habits for a keyboard that may feel better?

If you answered yes to 1 or 2 and 3, then I'd suggest a horizontal stagger, or no stagger at all if that appeals to you. If you answered a hard no for 3, I'd suggest going with a horizontal stagger.

### Split

Split keyboards have been around a while, though fully split keyboards were less common until a few years ago in favor of keyboards that were a single piece with the keys just split down the middle. The biggest problem with most split designs is a matter of which hand you use to type certain keys. For example, Many split keyboards put the number `6` on the right half, some put it on the left half, but if you type the number `6` with the other hand it can be a really big pain to switch because you're not just moving something a little, you're using a whole different hand and you have to develop new muscle memories for it.

The advantages of a split keyboard are pretty obvious though, being able to angle your wrists will put less stress on them long term. Also being able to spread your arms further apart may be more comfortable for you. It certainly is for me.

Consider the following:

1. Do you feel like your wrists are at odd angles, or do they get sore when you type?
2. Are you comfortable changing your typing habits for a keyboard that may feel better?

This time, if you answered "no" to number 2, it doesn't mean you won't be able to find something. I'd suggest checking which fingers you use to type `6` and `b`. And with those answers in mind, take a look at the available split keyboards and see if you can find one that meets your requirements. That may still require some adjustment, but it may be less than you expect. There is at least one keyboard that actually has a bit you can break off depending on which side those letters are on.

### Actually Picking A Layout

Once you've (hopefully) narrowed it down a bit, I'd suggest taking your keyboard into the [Keyboard Layout Editor](http://keyboard-layout-editor.com) and editing it further from there. If you're still on [Keyboards that Click](http://keyboardsthat.click), you can simply click on any layout and it'll take you to the layout editor for that keyboard.

From there you may want to re-arrange the keys, or even add keys! Once you've got it in a place you want, I'd recommend printing it off and just trying it out. Put your fingers on the layout and see how it feels to reach for the keys. From there you can tweak the layout further, or you can try something completely different. Either way, printing out a layout is an important step in figuring this out, since it lets you try things out before you totally commit, and while it's not the same as actually building something, it's a useful step to getting things done.

### Next up: Picking a Switch
