---
layout: post
title:  Moving to Gitlab pages
date:   2017-11-11 20:16
categories: code
author: Alex Ives
---
I've been using GitLab for the past year and a half at work and watched their product evolve delightfully over the last year and a half. Their unified product is pretty stellar. One of the coolest things they've done is their CI product. And with that, GitLab Pages. Unlike GitHub pages, GitLab uses their CI pipelines in order to do the build and deployment. Because of that not only are you not limited to the version of Jekyll that GitHub pages uses, you also have access to any other static site generator you want to use, so long as you can compile it in a pipleine.

There are some other pretty serious advantages to GitLab, for one, the 10GB repo size limit means that you could actually have a photo heavy site and not worry about it. For another, you can install a certificate for your custom domain giving you https. (I haven't yet) Pretty soon, you'll be able to run an automated job to update a let's encrypt certificate.

Also, because it's running the build in a CI pipeline, you can add dependent steps, like link validation and html proofing. Also, it's easy to see everything that goes on in the deployment simply by looking at [my gitlab ci file](https://gitlab.com/alexives/alexives.gitlab.io/blob/master/.gitlab-ci.yml). Pretty nifity!

Also, with the gitlab pages domains api merged into 10.2, you'll be able to use [Let's Encrypt](https://letsencrypt.org/) certificates and auto renew and update them them via CI.

If you don't have a GitHub page, or if you do and you want plugins or other things, take a look at GitLab's pages offering. I expect it'll be my go to from here on out if I need websites for things.