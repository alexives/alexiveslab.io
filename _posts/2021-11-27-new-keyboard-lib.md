---
layout: software
title:  "OpenSCAD Keyboard Library"
head_title: "OpenSCAD Keyboard Library"
date:   2021-11-27 12:44:00
categories: keyboards code
icon: images/scad_keyboard_lib_logo.png
author: Alex Ives
repo: https://gitlab.com/alexives/keyboard_lib/
latest_release: https://gitlab.com/alexives/keyboard_lib/-/releases
---

In the height of the pandemic I decided that I would get a 3D printer. I'd been thinking about it for a while and finally decided that I would just go for it and get one. I've been using a CNC for a while, but there are some advantages to being able to use a 3D printer instead since it can do shapes a little differently and it's a lot less work to just get something printed. Also since we just had a kid, having something that largely operates unattended is also advantageous.

I've been building keyboards for a while and in the past, I've either generated all of the cases and files by hand laying them out in fusion 360 and spacing with parameters, or generating case files with [swillkb](http://builder.swillkb.com/) and then adapting them in an svg editor like inkscape. But when I got the 3d printer I realized I would need to adapt my process in order to get the shapes to come out and I was tired of doing things non-programatically. This is where OpenSCAD came in! I started laying out files, manually, in openscad and extruding and subtracting and such so that I could generate the shapes. Then I realized that what I really wanted was an abstraction for keyswitch holes. Then I asked, "Wouldn't it be nice if I could just write out a list of the keys and their positions and have them lay out automatically?" Then I built an abstraction around different kinds of case files and generating cases.

That was when I realized that I had built what was basically a library and thought, hey, why not make it even more general and publish it! So here it is.

Currently there's still a lot of work the user has to do in order to make things come out. The case generation is haphazard at best and the api leaves something to be desired. Someday I hope to change to just taking [KLE](http://www.keyboard-layout-editor.com/) raw and using that to generate the files instead, but for now folks just have to figure it out.

Some advantages to this library
- Since all of the parts are generated from the same specs, they fit together pretty much without worrying
- I also use it to generate a set of files that define a pcb that can be etched on a CNC. (Hoping to someday generate gerber files maybe.)
- Because it's openscad just generating shapes, those shapes can be manpulated if you want to add your own case, or add parts to the case for styling or such.

I'd been working on designing a new version of my mittens keyboard and so I finally have a functional one thanks to this library! Also since I have a CNC, I could carve the PCB and it's _much_ more durable than my old keyboards. The pcb design is based on the [amoeba single switch pcb](https://github.com/mtl/keyboard-pcbs). The downside is you still have to do the work of manually wiring them, but since it's carved as a single piece you get the added stability and durability of traditional PCB.

Still working on the documentation so that it's easy to use, but in the mean time you can check out the readme to get a sense of how to use stuff, and here are some pics of the new keyboard!

![White plastic split ortholinear keyboard with blue and yellow keycaps in a grid, 24 keys on each half. Background is a blue desk pad with a line drawing of an astronaut.](/images/keyboards/mittens-3/top.jpg)

![Bottom of keyboard with copper colored plate with white circuit board traces, silver solder bridges and contacts, and colored wires connecting components together. White plastic case around the sides.](/images/keyboards/mittens-3/pcb.jpg)