---
layout: software
title:  "Timer workflow for Alfred 4"
head_title: "Timer workflow for Alfred 4"
date:   2019-12-03 14:16:42
categories: alfred code
icon: images/alfred-timer/logo.png
author: Alex Ives
repo: https://gitlab.com/alexives/timer-alfred-workflow/
latest_release: https://gitlab.com/alexives/timer-alfred-workflow/-/jobs/artifacts/master/download?job=build&file=Timer.alfredworkflow
---
Since I started working from home, I converted an old stationary bike into something I can use at my desk. I like to put a timer up so that I know I've been biking, or standing long enough (or if I've been sitting too long). So I built a workflow that allows you to set timers and view and cancel existing ones. It tells you it's done by putting large text on the screen saying it's done!

Setting a timer is easy, you just type `timer *d *h *m *s` into your alfred box. (replace the `*` with times for days, hours minutes seconds.)
![Timer set](/images/alfred-timer/timer-set.png)

Then you can view and cancel timers by typing `timers`, then ⌘+⏎ to remove/cancel the selected timer.
![Timer list view, says "timers" in the box, has a list with "09:58 Timer with 09:58 left, ⌘+⏎ to remove." and "239:59:47 Timer with 239:59:47 left, ⌘+⏎ to remove."](/images/alfred-timer/timers-list.png)

When the timer is done, it gives you a big image with the notification!
![Timer done view, says "10s timer is done!"](/images/alfred-timer/timer-done.png)
